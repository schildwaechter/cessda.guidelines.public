---
title: Version History
parent: Software Maturity Levels
published: 
nav_order: 493
---

# {{ page.title }}

| **Version**  | **Release Date**  | **Comment** |
|--------------|-------------------|-------------|
| 3.1.0 | June 2020 |Converted to Markdown format, and made available at <docs.tech.cessda.eu/sml/|
| 3.0.0 | March 2019 |Registered DOI via Zenodo |
| 2.1.0 |N/A | Added minimum and expected levels for CA8 |
| 2.0.0 | December 2018 | Following review by CESSDA CTO. |
| 1.2.0 | N/A | Moved to new document template. Extensive changes to sections CA5 and CA6. Minor modifications to CA11. Added section CA12. Now recognises importance of TRLs wrt to EOSC Hub. |
| 1.1.0 | N/A | Reviewed and updated all sections, including links. Global change from CRL to SML. |
| 1.0.0 | October 2017 | Extracted from Technical Architecture v1.0. |
