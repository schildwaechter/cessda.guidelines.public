---
title: Enhancing the Technical Architecture
parent: Architectural Principles
grand_parent: Technical Infrastructure
published: false
nav_order: 3103

---

# {{ page.title }}

A future version of the document must define a model for how to propose,
discuss and accept enhancements to the architecture.
Change requests are inevitable and there should be a transparent way to handle them.
Without such a governance model, it may appear that enhancement decisions are chaotic, whimsical, unfair, or unjustified.

In particular, changes to the scores for Minimum, Expected and Excellent standards for each
of the CESSDA Software use maturity level criteria must be agreed in an open manner and
communicated swiftly and effectively to SPs and other interested parties.
