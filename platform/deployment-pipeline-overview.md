---
title: Deployment Pipeline Overview
parent: Technical Infrastructure
nav_order: 325
---

# {{ page.title }}

## Terminology

See [Naming Conventions]({% link platform/naming-conventions.md %}).

## Overview

The diagram shows the deployment pipeline in CESSDA's Continuous Integration and Delivery process,
from the moment a developer commits code changes to the deployment of the  {% include glossary.html entry="(product)" text="product" %} to production.

![Deployment pipeline](../images/deployment-pipeline.png)
