# Changelog

All notable changes to the CESSDA Technical Guidelines will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 2020-12-01

* Improve documentation for documentation theme
* Add Matomo user statistics
* Revise Software Maturity Levels section to make it more product focused

## [1.0.0] - 2020-10-08

* Added contributor instructions to README
* Switched to CESSDA theme
* Clarified quality gates
* Added software development requirements
* Added release candidates and evaluation
* Defined service operation levels and requirements
* Added information on documentation
* Included platform documentation
* Include technical architecture description
* Add logging instructions
* Add documentation on authenticating with eduTEAMS
* Add documentation on the CESSDA branching model
* Extended Platform Team responsibilities
* Revised license requirements
* Included Technical Architecture documentation
* Added documentation on setting up oauth2-proxy

## [0.1.0] - 2020-06-22

* Use Jekyll to build static pages with the Just-the-docs-theme
* Deployment pipeline with markdown linting
* Use EURISE Technical Reference v0.2 as basis
* Defined quality gates
* Add release policy
* Add CONTRIBUTING template
* Define software quality requirements
* Define code archiving criteria
* Include import of Software Maturity Levels, [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.2614050.svg)](https://doi.org/10.5281/zenodo.2614050)

